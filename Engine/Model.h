#ifndef MODEL_H
#define MODEL_H

#include <GL\glew.h>
#include <string>
#include <vector>

#include <assimp\Importer.hpp>
#include <assimp\scene.h>
#include <assimp\postprocess.h>

#include "Shader.h"
#include "Mesh.h"

GLint TextureFromFile(const char* path, std::string directory);

class Model
{
public:
	/** Expects a file path to a 3D model */
	Model(GLchar* path);
	virtual ~Model();

	/** Draws the model, and all it's meshes */
	void Draw(Shader shader);
private:
	/** Model data */
	vector<Mesh> meshes;
	std::string directory;
	vector<Texture> textures_loaded;

	/** Load a model with supported ASSIMP extensions from file and stores the resulting meshes in the meshes vector */
	void LoadModel(std::string path);
	/** Processes a node in a recursive fashion. Process each individual mesh located at the node and repeats this process on its children nodes (if any) */
	void ProcessNode(aiNode* node, const aiScene* scene);
	Mesh ProcessMesh(aiMesh* mesh, const aiScene* scene);
	/** 
	 * Checks all material textures of a given type and loads the textures if they're not loaded yet.
	 * The required info is returned as a Texture struct.
	 */
	vector<Texture> LoadMaterialTextures(aiMaterial* material, aiTextureType type, std::string typeName);
};

#endif // !MODEL_H