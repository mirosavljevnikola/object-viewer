#ifndef MESH_H
#define MESH_H

#include <string>
#include <vector>

// GL includes
#include <GL\glew.h>
#include <glm\glm.hpp>

#include <assimp\Importer.hpp>

#include "Shader.h"

using namespace std;

struct Vertex
{
	glm::vec3 Position;
	glm::vec3 Normal;
	glm::vec2 TexCoords;
};

struct Texture
{
	GLuint id;
	aiString path;
	string type;
};

class Mesh
{
public:
	/** Mesh Data */
	vector<Vertex> vertices;
	vector<GLuint> indices;
	vector<Texture> textures;

	Mesh(vector<Vertex> vertices, vector<GLuint> indices, vector<Texture> textures);
	virtual ~Mesh();

	void Draw(Shader shader);
private:
	// Render data
	GLuint VAO, VBO, EBO;

	void SetupMesh();
};

#endif // !MESH_H