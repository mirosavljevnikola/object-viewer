#ifndef SHADER_H
#define SHADER_H

#include <string>
#include <fstream>
#include <sstream>
#include <iostream>

#include <GL\glew.h>

class Shader
{
public:
	Shader(const GLchar* vertexPath, const GLchar* fragmentPath);
	virtual ~Shader();
	// Use the program
	void Use();

	GLuint GetProgram() const { return program; }
protected:
private:
	GLuint program;
};

#endif // !SHADER_H